extends Node

export var log_file_path = "user://log.txt"
var log_file

func debug(data):
	_write('DEBUG:', data)

func info(data):
	_write('INFO:', data)

func warning(data):
	_write('WARNING:', data)

func error(data):
	_write('ERROR:', data)

func critical(data):
	_write('CRITICAL:', data)

func _init():
	# Open the log file.
	if log_file_path != "":
		log_file = File.new()
		if log_file.file_exists(log_file_path):
			_open_and_seek_to_end()
		else:
			log_file.open(log_file_path, File.WRITE)

func _open_and_seek_to_end():
	log_file.open(log_file_path, File.READ_WRITE)
	log_file.seek_end()

func _exit_tree():
	# Close the log file.
	if log_file != null:
		log_file.close()

func _get_time():
	var datetime = OS.get_datetime()
	return "[%d-%02d-%02d %02d:%02d:%02d]" % [datetime["year"],
			datetime["month"], datetime["day"], datetime["hour"],
			datetime["minute"], datetime["second"]]

func _write(type, data):
	var message = '%s %5s %s' % [_get_time(), type, data]

	if log_file != null:
		log_file.store_line(message)
		# Calling File.store_line() doesn't flush to file and it seems that
		# File API doesn't have a flush method on its own.
		# Fortunately, closing the file automatically flushes the written data
		# to file. But since this is a log file, we need to open it again.
		# Yes, this is a hack.
		log_file.close()
		_open_and_seek_to_end()
