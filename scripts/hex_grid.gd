extends Area

var hex_grid = {}
var size = Vector2(1, 1)
var grid_size = Vector2(13, 9)
var origin = Vector2(0, 0)
var triagles = PoolVector3Array()

onready var highlight = $Highlight
onready var map_node = get_parent().get_node("Map")

var is_multi_mesh_instance_visible = true

var cells = []

var upper_left_hex
var lower_left_hex
var upper_right_hex
var lower_right_hex

class HexData:
	var data
	var walls

func _ready():
	# Subscribe to events.
	event_bus.subscribe(events.grid_visiblity_changed, self, "_on_grid_visiblity_changed")
	event_bus.subscribe(events.create_new_map_called, self, "_on_create_new_map_called")
	# Highlight node is not part of the map editor data so remove it from that group.
	highlight.remove_from_group(groups.map_editor_data)
	generate_grid()

func _exit_tree():
	# Unsubscribe from events.
	event_bus.unsubscribe(events.grid_visiblity_changed, self)
	event_bus.unsubscribe(events.create_new_map_called, self)

func get_grid_size():
	return grid_size

func get_grid_size_in_px():
	var width = upper_right_hex.x - upper_left_hex.x
	var height = lower_left_hex.y - upper_left_hex.y
	return Vector2(width, height)

func _on_map_input_event(camera, event, click_position, click_normal, shape_idx):
	# Called when user moves a mouse over the collision plane.
	var plane_coords = Vector2(click_position.x, click_position.z)

	# Snap the highlight to the nearest grid cell.
	if highlight != null:
		var hex = pixel_to_hex(plane_coords)
		var plane_pos = hex_to_pixel(hex)
		highlight.translation.x = plane_pos.x
		highlight.translation.z = plane_pos.y

	# Check if the player click the left mouse button.
	if event is InputEventMouseButton:
		if event.button_index == BUTTON_LEFT and event.pressed:
			# Player has pressed left mouse button.
			# FIXME: Check if there is already a tile in the cell.
			var tile_base_scene = load("res://scenes/tile_base.tscn")
			var tile_base = tile_base_scene.instance()
			# Position the tile onto the location of the highlight node.
			tile_base.translation.x = highlight.translation.x
			tile_base.translation.z = highlight.translation.z
			# Place the tile node into the hex cell.
			map_node.add_child(tile_base)

func generate_grid():
	upper_left_hex = origin
	for i in range(grid_size.x):
		var i_offset = floor(i / 2)
		var j = -i_offset
		while j < grid_size.y - i_offset:
			var hex = origin + Vector2(i, j)
			add_hex(hex, null)
			var center = hex_to_pixel(hex)
			cells.append(center)
			generate_collision_shape_for_hex(hex)
			if i == 0 and j + 1 == grid_size.y - i_offset:
				lower_left_hex = center
			if i == grid_size.x -1 and j == -i_offset:
				upper_right_hex = center
			if i == grid_size.x -1 and j + 1 == grid_size.y - i_offset:
				lower_right_hex = center
			j += 1

	draw_hex_grid(hex_corners(origin), cells)

	var shape = ConcavePolygonShape.new()
	shape.set_faces(triagles)

	var collision = CollisionShape.new()
	collision.set_name("CollisionShape")
	collision.set_shape(shape)

	add_child(collision)

func draw_hex(hex):
	var corners = hex_corners(hex)
	for x in corners:
		var y = Vector3(x.x, 0, x.y)

func generate_collision_shape_for_hex(hex):
	var corners = hex_corners(hex)

	var t1 = PoolVector3Array()
	t1.append(Vector3(corners[0].x, 0.0, corners[0].y))
	t1.append(Vector3(corners[1].x, 0.0, corners[1].y))
	t1.append(Vector3(corners[2].x, 0.0, corners[2].y))
	triagles.append_array(t1)

	var t2 = PoolVector3Array()
	t2.append(Vector3(corners[2].x, 0.0, corners[2].y))
	t2.append(Vector3(corners[3].x, 0.0, corners[3].y))
	t2.append(Vector3(corners[4].x, 0.0, corners[4].y))
	triagles.append_array(t2)

	var t3 = PoolVector3Array()
	t3.append(Vector3(corners[4].x, 0.0, corners[4].y))
	t3.append(Vector3(corners[5].x, 0.0, corners[5].y))
	t3.append(Vector3(corners[0].x, 0.0, corners[0].y))
	triagles.append_array(t3)

	var t4 = PoolVector3Array()
	t4.append(Vector3(corners[0].x, 0.0, corners[0].y))
	t4.append(Vector3(corners[2].x, 0.0, corners[2].y))
	t4.append(Vector3(corners[4].x, 0.0, corners[4].y))
	triagles.append_array(t4)

func add_hex(hex, data):
	if (hex_grid.has(hex)):
		return
	var hd = HexData.new()
	hd.data = data
	hd.walls = []
	for i in range(5):
		hd.walls.push_back(null)
	hex_grid[hex] = hd

func get_wall(hex, direction):
	if (hex_grid.has(hex)):
		return hex_grid[hex].walls[direction]

func hex_to_pixel(hex):
	var x = ((3.0 / 2.0) * hex.x) * size.x
	var y = ((sqrt(3.0) / 2.0) * hex.x + sqrt(3.0) * hex.y) * size.y
	return Vector2(x + origin.x, y + origin.y)

func pixel_to_hex(pos):
	var pt = Vector2((pos.x - origin.x) / size.x,
					(pos.y - origin.y) / size.y)
	var q = (2.0 / 3.0) * pt.x
	var r = (-1.0 / 3.0) * pt.x + (sqrt(3.0) / 3.0) * pt.y
	return round_hex(Vector3(q, r, -q - r))

func round_hex(hex):
	var rx = round(hex.x)
	var ry = round(hex.y)
	var rz = round(hex.z)

	var x_diff = abs(rx - hex.x)
	var y_diff = abs(ry - hex.y)
	var z_diff = abs(rz - hex.z)

	if x_diff > y_diff and x_diff > z_diff:
		rx = -ry-rz
	elif y_diff > z_diff:
		ry = -rx-rz
	else:
		rz = -rx-ry

	return Vector3(rx, ry, rz)

func hex_corner_offset(corner):
	var angle = 2.0 * PI * corner / 6
	return Vector2(size.x * cos(angle), size.y * sin(angle))

func hex_corners(hex):
	var corners = []
	var center = hex_to_pixel(hex)
	for i in range(7):
		var offset = hex_corner_offset(i)
		corners.push_back(Vector2(center.x + offset.x, center.y + offset.y))
	return corners

func _on_grid_visiblity_changed(is_visible):
	# User wanted to changed the grid visibility.
	is_multi_mesh_instance_visible = is_visible
	$MultiMeshInstance.visible = is_visible

func _on_create_new_map_called(data):
	# User created a new empty map with specified dimensions.

	# Remove all tiles from the map.
	for tile in map_node.get_children():
		tile.queue_free()
	# Clear current grid.
	clear_grid()
	cells.clear()
	hex_grid.clear()
	triagles = PoolVector3Array()
	remove_child($CollisionShape)
	# Finally, set the grid size according to the user dimensions and generate the grid.
	grid_size = Vector2(data["width"], data["height"])
	generate_grid()
	# Reset the camera.
	var value = {
		"grid_size": grid_size,
		"upper_left_hex": upper_left_hex,
		"lower_left_hex": lower_left_hex,
		"upper_right_hex": upper_right_hex,
		"lower_right_hex": lower_right_hex
	}
	event_bus.publish(events.new_map_created, value)
	# Reposition highlight to the origin.
	highlight.translation.x = 0.0
	highlight.translation.z = 0.0

func draw_hex_grid(hex_corners, cells):
	var multi_mesh_instance = MultiMeshInstance.new()
	multi_mesh_instance.set_name("MultiMeshInstance")
	multi_mesh_instance.visible = is_multi_mesh_instance_visible
	add_child(multi_mesh_instance)

	var mesh = create_mesh_from_hex_corners(hex_corners)

	var mm = MultiMesh.new()
	mm.transform_format = MultiMesh.TRANSFORM_3D
	mm.instance_count = cells.size()
	mm.mesh = mesh

	for i in range(mm.instance_count):
		var pos = Vector3(cells[i].x, 0.0, cells[i].y)
		var t = Transform(Basis(), pos)
		mm.set_instance_transform(i, t)

	multi_mesh_instance.multimesh = mm

func create_mesh_from_hex_corners(hex_corners):
	var surface_tool = SurfaceTool.new()
	var mesh = Mesh.new()

	surface_tool.begin(Mesh.PRIMITIVE_LINE_STRIP)
	for corner in hex_corners:
		var vertex = Vector3(corner.x, 0, corner.y)
		surface_tool.add_vertex(vertex)
	mesh = surface_tool.commit(mesh)

	return mesh

func clear_grid():
	remove_child($MultiMeshInstance)

func get_upper_left_hex():
	return upper_left_hex

func get_lower_left_hex():
	return lower_left_hex

func get_upper_right_hex():
	return upper_right_hex

func get_lower_right_hex():
	return lower_right_hex
