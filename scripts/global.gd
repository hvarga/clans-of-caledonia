extends Node

const main_menu_scene = "res://scenes/main_menu.tscn"
const map_editor_scene = "res://scenes/map_editor.tscn"
const about_scene = "res://scenes/about.tscn"
const language_scene = "res://scenes/language.tscn"

const VERSION_FILE_PATH = "res://resources/build_version.json"

var build_version setget ,get_build_version
var build_date setget ,get_build_date

func _init():
	_parse_build_version_json()

func _parse_build_version_json():
	# Build version JSON file holds the version information gathered using Git
	# version control during the project build time. Parse the file if it
	# exists.
	logger.info("Parse build_version.json file.")
	var version_dict = {}
	var version_file = File.new()
	if version_file.file_exists(VERSION_FILE_PATH):
		# Build version file exists. Set the version and a date from the file.
		logger.debug("build_version.json file exists. Read its contents.")
		version_file.open(VERSION_FILE_PATH, File.READ)
		var version_text = version_file.get_as_text()
		version_dict = parse_json(version_text)
		logger.info("Build version info: Version: %s, Date: %s." %
				[version_dict["version"], version_dict["date"]])
		version_file.close()
	else:
		# Build version is missing from the filesystem. Set the version and
		# date to an empty values. This is not some invalid state, it simply
		# means that the project is not been built using provided Makefile.
		logger.debug("build_version.json file is missing from resources.")
		version_dict["version"] = null
		version_dict["date"] = null

	build_version = version_dict["version"]
	build_date = version_dict["date"]

func get_build_version():
	return build_version

func get_build_date():
	return build_date

func get_project_name():
	return ProjectSettings.get_setting("application/config/name")
