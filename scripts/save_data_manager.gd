extends Node

var map_file_pass = "2t3aeyhy9eYjpC01xNM9xhCqb4aYNUCrH92xs3kPWe24sM7u"

func save_map_data(scene_tree, file_path):
	# Get all the nodes that hold the map editor data.
	var nodes = scene_tree.get_nodes_in_group(groups.map_editor_data)
	logger.debug("Found %d number of map editor data nodes to save." % [len(nodes)])
	# Set the map editor data header.
	var grid_size = scene_tree.get_root().get_node("MapEditor/HexGrid").get_grid_size()
	var save_data = {
		"version": global.build_version,
		"grid_width": grid_size.x,
		"grid_height": grid_size.y,
		"nodes": {}
	}
	# Get the data for each node.
	for node in nodes:
		var node_name = node.get_name()
		var node_data = node.get_map_editor_data()
		save_data.nodes[node_name] = node_data
	# Finally, save map editor data to a file.
	var save_data_file = File.new()
	save_data_file.open_encrypted_with_pass(file_path, File.WRITE, map_file_pass)
	save_data_file.store_string(to_json(save_data))
	save_data_file.close()

func load_map_data(parent_node, file_path):
	var map_data_file = File.new()
	if not map_data_file.file_exists(file_path):
		# File doesn't exist. Just return, for now.
		return 1
	# Read the map data file.
	map_data_file.open_encrypted_with_pass(file_path, File.READ, map_file_pass)
	var map_data = parse_json(map_data_file.get_as_text())
	if not map_data:
		event_bus.publish(events.pause_enabled)
		var error_message = tr("MAP_EDITOR_LOAD_MAP_INVALID_FILE_TEXT")
		error_message = error_message % [file_path]
		logger.error(error_message)
		var error_dialog = AcceptDialog.new()
		error_dialog.set_title(tr("MAP_EDITOR_LOAD_MAP_INVALID_FILE_TITLE"))
		error_dialog.set_text(error_message)
		error_dialog.set_pause_mode(Node.PAUSE_MODE_PROCESS)
		error_dialog.set_exclusive(true)
		var error_dialog_parent = parent_node.get_tree().get_root().get_node("MapEditor/GUI")
		error_dialog_parent.add_child(error_dialog)
		error_dialog.popup_centered()
		# Wait for user to close the dialog.
		yield(error_dialog, "popup_hide")
		error_dialog.queue_free()
		event_bus.publish(events.pause_disabled)
		return 1
	# Generate a new, empty grid.
	var value = {
		"width": map_data["grid_width"],
		"height": map_data["grid_height"]
	}
	event_bus.publish(events.create_new_map_called, value, true)
	# Iterate over dictionary and instance all necessery nodes under parent.
	var map_nodes = map_data["nodes"]
	for node_name in map_nodes:
		var node_data = map_nodes[node_name]
		var node_scene = load(node_data["filename"])
		var node = node_scene.instance()
		node.translation.x = node_data["translation_x"]
		node.translation.z = node_data["translation_z"]
		parent_node.add_child(node)

	return 0