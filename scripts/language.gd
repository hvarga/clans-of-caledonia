extends Control

onready var language_list = $"VBoxContainer/LanguageList"

func _ready():
	# Add supported languages.
	_add_supported_languages()
	# Select current language in the language list.
	_select_current_language()
	# Show language dialog.
	popup_centered()
	# Resume execution when dialog is hidden.
	yield(self, "popup_hide")
	queue_free()

func _on_cancel_button_pressed():
	# Called when user presses on cancel button.
	# This signifies that user want's to cancel choosing of a prefered language.
	# Do nothing and close the dialog.
	hide()

func _on_item_activated(index):
	# Called when user double clicks or selects and presses enter on a language.
	var language_name = language_list.get_item_text(index)
	_apply_language(language_name)

func _on_ok_button_pressed():
	# Called when user presses on ok button.
	var selected_items = language_list.get_selected_items()
	var language_name = language_list.get_item_text(selected_items[0])
	_apply_language(language_name)

func _apply_language(language):
	settings.set_language(language)
	TranslationServer.set_locale(settings.get_supported_languages()[language])
	logger.info("Language %s applied." % language)
	hide()

func _add_supported_languages():
	for language in settings.get_supported_languages():
		language_list.add_item(language)

func _select_current_language():
	#var locale = TranslationServer.get_locale()
	#var locale_name = TranslationServer.get_locale_name(locale)
	var language = settings.get_language()
	for i in range(settings.get_supported_languages().size()):
		var language_name = language_list.get_item_text(i)
		if language == language_name:
			language_list.select(i)
