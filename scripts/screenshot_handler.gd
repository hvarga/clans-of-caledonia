extends Node

export var screenshot_folder = "user://screenshots/"

func _ready():
	# Check if screenshot folder exist, and create it if doesn't.
	var directory = Directory.new()
	if not directory.dir_exists(screenshot_folder):
		directory.make_dir(screenshot_folder)

func _input(event):
	if(event.is_action_pressed("take_screenshot")):
		# Retrieve the captured image from the viewport.
		var screenshot = get_viewport().get_texture().get_data()
		# Flip it on the y-axis (because it's flipped for some reason).
		screenshot.flip_y()
		# Save in-memory screenshot to a filesystem.
		var file = screenshot_folder + "screenshot_" + self._get_time() + ".png"
		var error = screenshot.save_png(file)
		if error != OK:
			logger.error("Error taking screenshot. Error code: %d." % error)
		else:
			logger.info("Screenshot '%s' taken." % ProjectSettings.globalize_path(file))

func _get_time():
	var datetime = OS.get_datetime()
	return "%d-%02d-%02d_%02d-%02d-%02d" % [datetime["year"],
			datetime["month"], datetime["day"], datetime["hour"],
			datetime["minute"], datetime["second"]]