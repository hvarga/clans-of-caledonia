extends HBoxContainer

enum FileMenuItems {
	NEW_MAP = 0
	SAVE_MAP
	SAVE_MAP_AS
	LOAD_MAP
	QUIT_EDITOR
	EXIT_GAME
}

enum ViewMenuItems {
	SHOW_GRID = 0
	RESET_CAMERA
}

enum ToolsMenuItems {
	TAKE_SCREENSHOT = 10
}

onready var file_menu_popup = get_node("FileMenu").get_popup()
onready var view_menu_popup = get_node("ViewMenu").get_popup()
onready var tools_menu_popup = get_node("ToolsMenu").get_popup()
onready var map_node = get_tree().get_root().get_node("MapEditor/Map")
onready var hex_grid_node = get_tree().get_root().get_node("MapEditor/HexGrid")
onready var window_node = get_tree().get_root().get_node("MapEditor/GUI/Window")

var current_dir = null
var current_loaded_map = null

func _ready():
	# Subscribe to events.
	event_bus.subscribe(events.create_new_map_called, self, "_on_create_new_map_called")
	# Add all file menu items.
	file_menu_popup.add_item("MAP_EDITOR_FILE_NEW_MAP", FileMenuItems.NEW_MAP)
	file_menu_popup.add_item("MAP_EDITOR_FILE_SAVE_MAP", FileMenuItems.SAVE_MAP)
	file_menu_popup.add_item("MAP_EDITOR_FILE_SAVE_MAP_AS", FileMenuItems.SAVE_MAP_AS)
	file_menu_popup.add_item("MAP_EDITOR_FILE_LOAD_MAP", FileMenuItems.LOAD_MAP)
	file_menu_popup.add_separator()
	file_menu_popup.add_item("MAP_EDITOR_FILE_QUIT_EDITOR", FileMenuItems.QUIT_EDITOR)
	file_menu_popup.add_item("MAP_EDITOR_FILE_EXIT_GAME", FileMenuItems.EXIT_GAME)
	# Connect the signal to when user press on any file menu item.
	file_menu_popup.connect("id_pressed", self, "_on_file_menu_item_pressed")

	# Add all view menu items.
	view_menu_popup.add_check_item("MAP_EDITOR_VIEW_SHOW_GRID", ViewMenuItems.SHOW_GRID)
	view_menu_popup.set_item_checked(ViewMenuItems.SHOW_GRID, true)
	view_menu_popup.add_item("MAP_EDITOR_VIEW_RESET_CAMERA", ViewMenuItems.RESET_CAMERA)
	# Connect the signal to when user press on any view menu item.
	view_menu_popup.connect("id_pressed", self, "_on_view_menu_item_pressed")

	# Add all tools menu items.
	tools_menu_popup.add_item("MAP_EDITOR_TOOLS_TAKE_SCREENSHOT", ToolsMenuItems.TAKE_SCREENSHOT)
	# Connect the signal to when user press on any tools menu item.
	tools_menu_popup.connect("id_pressed", self, "_on_tools_menu_item_pressed")

func _on_create_new_map_called(event):
	# Called when new map is created.
	current_loaded_map = null
	event_bus.publish(events.current_loaded_map_changed, "") # FIXME: We need to send the null
	# but our current EventBus implementation doesn't allow that. Maybe it is better to just remove EventBus
	# and use Godot existing groups and signals feature instead.

func _on_file_menu_item_pressed(id):
	# Called when user makes a press on any file menu item.
	# Differentiate each menu item by its id.
	logger.info("File menu item pressed.")
	match id:
		FileMenuItems.NEW_MAP:
			logger.info("New map requested.")
			var new_map_dialog = preload("res://scenes/new_map.tscn").instance()
			var grid_size = hex_grid_node.get_grid_size()
			new_map_dialog.init(grid_size)
			window_node.add_child(new_map_dialog)
		FileMenuItems.QUIT_EDITOR:
			logger.info("Return to main menu.")
			get_tree().change_scene(global.main_menu_scene)
		FileMenuItems.EXIT_GAME:
			logger.info("Exit game.")
			get_tree().quit()
		FileMenuItems.SAVE_MAP:
			if current_loaded_map:
				_save_map_data(current_loaded_map)
			else:
				_save_map_as()
		FileMenuItems.SAVE_MAP_AS:
			_save_map_as()
		FileMenuItems.LOAD_MAP:
			# Load the map data from the file.
			logger.info("Load map.")
			event_bus.publish(events.pause_enabled)
			# Open up a load file dialog and ask a player for a file which contains map data.
			var load_file_dialog = FileDialog.new()
			load_file_dialog.add_filter("*.cocmap ; Clans of Caledonia Map");
			load_file_dialog.set_size(get_viewport().size / 3.0)
			load_file_dialog.set_pause_mode(Node.PAUSE_MODE_PROCESS)
			load_file_dialog.set_exclusive(true)
			load_file_dialog.set_resizable(true)
			load_file_dialog.set_mode(FileDialog.MODE_OPEN_FILE)
			load_file_dialog.set_access(FileDialog.ACCESS_FILESYSTEM)
			load_file_dialog.connect("file_selected", self, "_on_load_map_dialog_file_selected", [load_file_dialog])
			if current_dir:
				load_file_dialog.set_current_dir(current_dir)
			window_node.add_child(load_file_dialog)
			load_file_dialog.popup_centered()
			yield(load_file_dialog, "popup_hide")
			load_file_dialog.queue_free()
			event_bus.publish(events.pause_disabled)

func _save_map_as():
	# Save the map data to a file which can be loaded later on when needed.
	logger.info("Save map.")
	event_bus.publish(events.pause_enabled)
	# Open up a save file dialog and ask a player where he wants to save the map.
	var save_file_dialog = FileDialog.new()
	save_file_dialog.add_filter("*.cocmap ; Clans of Caledonia Map");
	save_file_dialog.set_size(get_viewport().size / 3.0)
	save_file_dialog.set_pause_mode(Node.PAUSE_MODE_PROCESS)
	save_file_dialog.set_exclusive(true)
	save_file_dialog.set_resizable(true)
	save_file_dialog.set_access(FileDialog.ACCESS_FILESYSTEM)
	save_file_dialog.connect("file_selected", self, "_on_save_map_dialog_file_selected", [save_file_dialog])
	if current_dir:
		save_file_dialog.set_current_dir(current_dir)
	window_node.add_child(save_file_dialog)
	save_file_dialog.popup_centered()
	yield(save_file_dialog, "popup_hide")
	save_file_dialog.queue_free()
	event_bus.publish(events.pause_disabled)

func _save_map_data(file_path):
	var save_data_manager = load("res://scripts/save_data_manager.gd").new()
	save_data_manager.save_map_data(get_tree(), file_path)

func _on_save_map_dialog_file_selected(file_path, file_dialog):
	# Called when player selects a file into which he wants to save map data.
	# Call save data manager.
	_save_map_data(file_path)
	current_dir = file_dialog.get_current_dir()
	current_loaded_map = file_path
	event_bus.publish(events.current_loaded_map_changed, file_path)

func _on_load_map_dialog_file_selected(file_path, file_dialog):
	# Called when player selects a map file which he wants to load into map editor.
	var save_data_manager = load("res://scripts/save_data_manager.gd").new()
	var err = save_data_manager.load_map_data(map_node, file_path)
	if err:
		return

	current_dir = file_dialog.get_current_dir()
	current_loaded_map = file_path
	event_bus.publish(events.current_loaded_map_changed, file_path)

func _on_view_menu_item_pressed(id):
	# Called when user makes a press on any view menu item.
	# Differentiate each menu item by its id.
	logger.info("View menu item pressed.")
	match id:
		ViewMenuItems.SHOW_GRID:
			logger.info("Show/hide grid.")
			var is_checked = view_menu_popup.is_item_checked(ViewMenuItems.SHOW_GRID)
			view_menu_popup.set_item_checked(ViewMenuItems.SHOW_GRID, !is_checked)
			event_bus.publish(events.grid_visiblity_changed, !is_checked)
		ViewMenuItems.RESET_CAMERA:
			logger.info("Reset camera.")
			var value = {
				"grid_size": hex_grid_node.get_grid_size(),
				"upper_left_hex": hex_grid_node.get_upper_left_hex(),
				"lower_left_hex": hex_grid_node.get_lower_left_hex(),
				"upper_right_hex": hex_grid_node.get_upper_right_hex(),
				"lower_right_hex": hex_grid_node.get_lower_right_hex()
			}
			event_bus.publish(events.reset_camera_called, value)

func _on_tools_menu_item_pressed(id):
	# Called when user makes a press on any tools menu item.
	# Differentiate each menu item by its id.
	logger.info("Tools menu item pressed.")
	match id:
		ToolsMenuItems.TAKE_SCREENSHOT:
			logger.info("Take screenshot.")
			var event = InputEventAction.new()
			event.set_action("take_screenshot")
			event.set_pressed(true)
			Input.parse_input_event(event)

func _exit_tree():
	# Unsubscribe from events.
	event_bus.unsubscribe(events.create_new_map_called, self)