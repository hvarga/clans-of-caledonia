extends Node

# PauseHandler is used to pause the game engine.
# It is a wrapper around @SceneTree.paused and MUST be used instead.
# The idea is to control pausing of the game in one central place to avoid a situations where
# more than one game module pauses the game at the same time when another wants to resume the game.

# It is implemented as a reference counter similar to @Reference but instead of destroying an object,
# PauseHandler can pause or resume the game based on the counter value.

var counter_lock = Mutex.new()
var counter = 0
var scene_tree

func _ready():
	scene_tree = get_tree()
	# Subscribe to events.
	event_bus.subscribe(events.pause_enabled, self, "_on_pause_enabled")
	event_bus.subscribe(events.pause_disabled, self, "_on_pause_disabled")

func _exit_tree():
	# Unsubscribe from events.
	event_bus.unsubscribe(events.pause_enabled, self)
	event_bus.unsubscribe(events.pause_disabled, self)

func _on_pause_enabled():
	# Called when a game module requests to pause the game engine.
	# Note that a module requesting to pause the game engine, needs to whitelist itself from pausing
	# using @Node.set_pause_mode(Node.PAUSE_MODE_PROCESS) or else it will be paused also.
	counter_lock.lock()
	counter += 1
	counter_lock.unlock()

	# Check to see if anybody else still want's to game engine to be paused.
	if counter > 0:
		scene_tree.paused = true

func _on_pause_disabled():
	# Called when a game module requests to resume the game engine.
	counter_lock.lock()
	counter -= 1
	counter_lock.unlock()

	# Check if no one else requested to pause the game engine before resuming.
	if counter == 0:
		scene_tree.paused = false