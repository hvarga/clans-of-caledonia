extends WindowDialog

onready var width_value = $VerticalBoxContainer/GridContainer/WidthValue
onready var height_value = $VerticalBoxContainer/GridContainer/HeightValue

onready var width_slider = $VerticalBoxContainer/GridContainer/WidthSlider
onready var height_slider = $VerticalBoxContainer/GridContainer/HeightSlider

var default_grid_size = Vector2(13, 9)

func init(current_grid_size):
	# Set the sliders default values before the dialog pops.
	# Default values are set to the size of the current grid.
	default_grid_size = current_grid_size

func _ready():
	event_bus.publish(events.pause_enabled)
	# Set the default values for width and height according to the size of the
	# current grid.
	width_slider.set_value(default_grid_size.x)
	width_value.value = default_grid_size.x
	height_slider.set_value(default_grid_size.y)
	height_value.value = default_grid_size.y
	# Show dialog.
	popup_centered()

func _on_create_button_pressed():
	# Get the value of width and height.
	var value = {
		"width": width_slider.value,
		"height": height_slider.value
	}
	# Notify everyone that user wants to create a new map with the choosen width and height.
	event_bus.publish(events.create_new_map_called, value)
	_close_dialog()

func _on_cancel_button_pressed():
	# User pressed cancel button. Just close the dialog.
	_close_dialog()

func _on_popup_hide():
	# User pressed on X button on top right. Just close the dialog.
	_close_dialog()

func _close_dialog():
	event_bus.publish(events.pause_disabled)
	queue_free()

func _on_width_slider_value_changed(value):
	# Called when user changes the value of a width slider.
	width_value.value = value

func _on_height_slider_value_changed(value):
	# Called when user changes the value of a height slider.
	height_value.value = value

func _on_height_value_value_changed(value):
	# Called when user changes the value of a height spin box.
	height_slider.set_value(value)

func _on_width_value_value_changed(value):
	# Called when user changes the value of a width spin box.
	width_slider.set_value(value)
