extends Control

func _ready():
	$CenterContainer/VerticalBoxContainer/GameTitleLabel.text = global.get_project_name()
	logger.info("Main menu loaded.")

func _on_button_map_editor_pressed():
	logger.info("Load map editor.")
	get_tree().change_scene(global.map_editor_scene)

func _on_button_exit_pressed():
	# User has pressed "Exit Game" button from the main menu.
	# Quit the game and return control to the operating system.
	logger.info("Exit game.")
	get_tree().quit()

func _on_about_button_pressed():
	# Called when user presses about button from the main menu.
	var about_scene = load(global.about_scene)
	var about = about_scene.instance()
	var main_menu = get_node(".")
	main_menu.add_child(about)

func _on_language_button_pressed():
	# Called when a player presses on a language button in the main menu.
	var language_scene = load(global.language_scene)
	var language = language_scene.instance()
	add_child(language)