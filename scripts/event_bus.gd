extends Node

var receivers = {}
var events = []

var events_lock = Mutex.new()
var receivers_lock = Mutex.new()

class Event:
	var receiver = null
	var method = null
	var value = null

func _process(delta):
	_process_events()

func _process_events():
	if events.size() == 0:
		# There are no events to process. Just return.
		return

	for i in range(events.size()):
		events_lock.lock()
		var event = events.pop_front()
		events_lock.unlock()

		if event.value != null:
			event.receiver.call(event.method, event.value)
		else:
			event.receiver.call(event.method)

func publish(event_name, value = null, immediate = false):
	# Publish an event to enyone who is willing to listen.
	# Event will actually be queued and processed on very next frame.
	# If necessery, the event can be published immediately, without queuing, by setting immediate to true.

	if not receivers.has(event_name):
		# There is no one who is listening for this type of event.
		# Just return.
		return
	var event_receivers = receivers[event_name]
	for receiver in event_receivers.keys():
		var event = Event.new()
		event.receiver = receiver
		event.method = event_receivers[receiver]
		event.value = value

		if immediate:
			event.receiver.call(event.method, event.value)
		else:
			events_lock.lock()
			events.push_back(event)
			events_lock.unlock()

func subscribe(event_name, receiver, method):
	# Subscribe an object to an event.
	# Object needs to unsubscribe from EventBus before it is destroy by calling @EventBus.unsubscribe().
	if not receiver.has_method(method):
		# Object does't have a method implemented.
		# Log it as a warning and return.
		logger.error("Receiver %s doesn't have method called %s." % [receiver.get_name(), method])
		return

	receivers_lock.lock()
	if not receivers.has(event_name):
		receivers[event_name] = {}
	receivers_lock.unlock()
	receivers[event_name][receiver] = method

func unsubscribe(event_name, receiver):
	# Unsubscribe an object from receiving event.
	receivers[event_name].erase(receiver)