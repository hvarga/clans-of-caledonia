extends WindowDialog

func _ready():
	# Populate the version labels.
	_set_version_information()
	# Populate the settings file path label.
	_set_data_directory_path()
	# Populate project title.
	_set_title_label()
	# Show about dialog.
	popup_centered()

func _set_version_information():
	var build_version = global.get_build_version()
	if build_version:
		$VerticalBoxContainer/PanelContainer/VersionValueLabel.text = build_version

	var build_date = global.get_build_date()
	if build_date:
		$VerticalBoxContainer/PanelContainer/DateValueLabel.text = build_date

func _set_data_directory_path():
	$VerticalBoxContainer/PanelContainer/DataDirectoryValue.text = ProjectSettings.globalize_path("user://")

func _set_title_label():
	$VerticalBoxContainer/PanelContainer/TitleValueLabel.text = global.get_project_name()

func _on_close_button_pressed():
	# User pressed close button. Just close the about dialog.
	_close_dialog()

func _on_about_popup_hide():
	# User pressed on X button on top right. Just close the about dialog.
	_close_dialog()

func _close_dialog():
	queue_free()
