extends Node

# Game events.

# Event new_map_created is published exactly when a new map is created.
const new_map_created = "new_map_created"
# Event create_new_map_called is published before a new map is created
# signaling that the player intention is to create a new map.
const create_new_map_called = "create_new_map_called"
# Event grid_visiblity_changed is published after player changed the grid
# visibility.
const grid_visiblity_changed = "grid_visiblity_changed"
# Event reset_camera_called is published after player decided that he want
# to reset the camera.
const reset_camera_called = "reset_camera_called"
const pause_enabled = "pause_enabled"
const pause_disabled = "pause_disabled"
const current_loaded_map_changed = "map_loaded"