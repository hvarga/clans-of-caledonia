extends BoxContainer

onready var map_file_value = $"File/Value"

func _ready():
	event_bus.subscribe(events.current_loaded_map_changed, self, "_on_current_loaded_map_changed")
	map_file_value.text = "*"

func _on_current_loaded_map_changed(path):
	if path == "":
		map_file_value.text = "*"
	else:
		map_file_value.text = path

func _exit_tree():
	# Unsubscribe from events.
	event_bus.unsubscribe(events.current_loaded_map_changed, self)