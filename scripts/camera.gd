extends Camera

onready var Yaw = get_parent()
onready var hex_grid_node = get_tree().get_root().get_node("MapEditor/HexGrid")

var zoom_speed = 30
var rotation_speed = 500

var vertical_rotation_min = deg2rad(-90)
var vertical_rotation_max = deg2rad(0)

var yaw_center_position = Vector2()
var yaw_max_zoom = 0
var yaw_min_zoom = 2
var pan_limit = Vector2()

var pressed = false

# Camera pan parameters.
export var acceleration = 0.05
export var max_speed = 100
var speed_multiplier = 5
var motion = Vector3()
export var use_smoothing = true

# Camera zoom parameters.
var zoom_motion = 0
var zoom_step_amount = 0
var zoom_steps = 4

# Camera rotation.
var rotation_motion = Vector2()

func _ready():
	# Subscribe to events.
	event_bus.subscribe(events.new_map_created, self, "_on_new_map_created")
	event_bus.subscribe(events.reset_camera_called, self, "_on_reset_camera_called")
	# Set initial position.
	var value = {
		"grid_size": hex_grid_node.get_grid_size(),
		"upper_left_hex": hex_grid_node.get_upper_left_hex(),
		"lower_left_hex": hex_grid_node.get_lower_left_hex(),
		"upper_right_hex": hex_grid_node.get_upper_right_hex(),
		"lower_right_hex": hex_grid_node.get_lower_right_hex()
	}
	_reset_camera(value)
	_set_camera_pan_limit(hex_grid_node.get_grid_size_in_px())

func _set_camera_pan_limit(grid_size_in_px):
	# Set the pan and zoom camera limits. This is so that the player is not able
	# to pan or zoom the camera indefinitely and to zoom through the grid and tiles.
	pan_limit.x = grid_size_in_px.x
	pan_limit.y = grid_size_in_px.y

func _exit_tree():
	# Unsubscribe from events.
	event_bus.unsubscribe(events.new_map_created, self)
	event_bus.unsubscribe(events.reset_camera_called, self)

func get_vertical_rotation(mouse_position = 0):
	var rotation = self.get_rotation() + Vector3(mouse_position, 0, 0)
	rotation.x = clamp(rotation.x, vertical_rotation_min, vertical_rotation_max)

	return rotation

func get_horizontal_rotation(mouse_position = 0):
	return Yaw.get_rotation() + Vector3(0, mouse_position, 0)

func _input(event):
	if Input.is_action_pressed('zoom_up'):
		zoom_motion = Yaw.translation.y - (zoom_step_amount + (Yaw.translation.y - zoom_motion))
	elif Input.is_action_pressed("zoom_down"):
		zoom_motion = Yaw.translation.y + (zoom_step_amount + (zoom_motion - Yaw.translation.y))

	zoom_motion = clamp(zoom_motion, yaw_min_zoom, yaw_max_zoom)

	if event is InputEventMouseButton:
		if event.button_index == BUTTON_RIGHT and event.is_pressed():
			pressed = true
		elif event.button_index == BUTTON_RIGHT and not event.is_pressed():
			pressed = false

	if event is InputEventMouseMotion and pressed:
		rotation_motion = event.relative

func _process(delta):
	# Rotate the camera.
	if use_smoothing:
		rotation_motion.y = lerp(rotation_motion.y, 0, 0.08)
		rotation_motion.x = lerp(rotation_motion.x, 0, 0.08)
	self.set_rotation(get_vertical_rotation(rotation_motion.y / -rotation_speed))
	Yaw.set_rotation(get_horizontal_rotation(rotation_motion.x / -rotation_speed))
	if not use_smoothing:
		rotation_motion.y = 0
		rotation_motion.x = 0

	# Zoom the camera.
	if use_smoothing:
		Yaw.translation.y = lerp(Yaw.translation.y, zoom_motion, 0.1)
	else:
		Yaw.translation.y = zoom_motion

	# Pan the camera.
	if Input.is_action_pressed('ui_left'):
		if use_smoothing:
			motion.x = lerp(motion.x, -max_speed * speed_multiplier, acceleration)
		else:
			motion.x = -max_speed * speed_multiplier
	elif Input.is_action_pressed('ui_right'):
		if use_smoothing:
			motion.x = lerp(motion.x, max_speed * speed_multiplier, acceleration)
		else:
			motion.x = max_speed * speed_multiplier
	else:
		if use_smoothing:
			motion.x = lerp(motion.x, 0, acceleration)
		else:
			motion.x = 0
	if Input.is_action_pressed('ui_up'):
		if use_smoothing:
			motion.z = lerp(motion.z, -max_speed * speed_multiplier, acceleration)
		else:
			motion.z = -max_speed * speed_multiplier
	elif Input.is_action_pressed('ui_down'):
		if use_smoothing:
			motion.z = lerp(motion.z, max_speed * speed_multiplier, acceleration)
		else:
			motion.z = max_speed * speed_multiplier
	else:
		if use_smoothing:
			motion.z = lerp(motion.z, 0, acceleration)
		else:
			motion.z = 0

	motion.x = clamp(motion.x, -max_speed * speed_multiplier, max_speed * speed_multiplier)
	motion.z = clamp(motion.z, -max_speed * speed_multiplier, max_speed * speed_multiplier)

	var yaw_forward = Yaw.get_global_transform().basis.z
	var velocity = yaw_forward * motion.z
	var camera_side = get_global_transform().basis.x
	velocity += camera_side * motion.x
	Yaw.global_translate(velocity * delta)
	# Limit the camera pan and zoom.
	# FIXME: Pan limit feels a bit clunky. Maybe use limit around a circle instead of the box.
	# But then again, a lot of strategy games have the same box-clamping of the camera.
	Yaw.translation.x = clamp(Yaw.translation.x, yaw_center_position.x - pan_limit.x, yaw_center_position.x + pan_limit.x)
	Yaw.translation.z = clamp(Yaw.translation.z, yaw_center_position.y - pan_limit.y, yaw_center_position.y + pan_limit.y)
	Yaw.translation.y = clamp(Yaw.translation.y, yaw_min_zoom, yaw_max_zoom)

func _on_new_map_created(event):
	# New map was created, reposition the camera to the map origin.
	_reset_camera(event)
	_set_camera_pan_limit(hex_grid_node.get_grid_size_in_px())

func _on_reset_camera_called(event):
	# Called when player presses on reset camera.
	_reset_camera(event)
	_set_camera_pan_limit(hex_grid_node.get_grid_size_in_px())

func _reset_camera(event):
	# Calculate hex grid center point.
	var x
	var y
	# Check if the grid is one dimensional.
	if event["grid_size"].x == 1 or event["grid_size"].y == 1:
		# The grid is one dimensional.
		# This means that we don't need to calculate intersection of two lines at all.
		# There is just one line and the middle point of this line is a center of the grid.
		x = (event["upper_left_hex"].x + event["lower_right_hex"].x) / 2
		y = (event["upper_left_hex"].y + event["lower_right_hex"].y) / 2
	else:
		# The grid is two dimensional.
		# The center point can be calculated from the intersection of two lines (grid diagonals).
		# Lines or diagonals are each defined by two points.
		# First line or diagonal is defined by upper_left_hex and lower_right_hex, and the second diagonal
		# is defined by lower_left_hex and upper_right_hex.
		# Calculate slope and y-interestion for each of the two diagonals.

		# First diagonal.
		var slope1 = (event["lower_right_hex"].y - event["upper_left_hex"].y) / (event["lower_right_hex"].x - event["upper_left_hex"].x)
		var y_intersection1 = -slope1 * event["upper_left_hex"].x + event["upper_left_hex"].y
		# Second diagonal.
		var slope2 = (event["upper_right_hex"].y - event["lower_left_hex"].y) / (event["upper_right_hex"].x - event["lower_left_hex"].x)
		var y_intersection2 = -slope2 * event["lower_left_hex"].x + event["lower_left_hex"].y
		# Calculate the intersection of two diagonals.
		x = (y_intersection2 - y_intersection1) / (slope1 - slope2)
		y = slope1 * x + y_intersection1

	# Rotate the camera to look straight down on hex grid (90 degree) and align horizontaly.
	Yaw.set_rotation_degrees(Vector3(0.0, 0.0, 0.0))
	self.set_rotation(Vector3(vertical_rotation_min, 0.0, 0.0))

	# Position camera to the point where two diagonals intersect.
	Yaw.translation.x = x
	Yaw.translation.z = y

	yaw_center_position.x = x
	yaw_center_position.y = y

	# Zoom the camera such that the whole hex grid fits into view.
	var opposite
	# Need to find the longest opposite cathetus from which the zoom value can be calculated.
	if x < y:
		opposite = y
	else:
		opposite = x
	var zoom = opposite / tan(deg2rad(fov / 2))
	var camera_forward = get_global_transform().basis.z
	Yaw.translation.y = zoom + 10
	yaw_max_zoom = Yaw.translation.y
	zoom_motion = Yaw.translation.y
	zoom_step_amount = (yaw_max_zoom - yaw_min_zoom) / zoom_steps
