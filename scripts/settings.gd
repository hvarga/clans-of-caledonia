extends Node

export var settings_file = "user://settings.cfg"
var settings = null

var supported_languages = {
	"English": "en",
	"Croatian": "hr"
}

func _ready():
	settings = ConfigFile.new()
	_load_settings()

func _load_settings():
	# Check that settings file exists on file system.
	var file = File.new()
	if not file.file_exists(settings_file):
		# Settings file doesn't exist. Try to create a new empty file.
		file.open(settings_file, File.WRITE)
		file.close()
	# Try to load the settings file.
	var err = settings.load(settings_file)
	if err != OK:
		logger.error("Unable to load settings file %s" % settings_file)
		return

	_appy_settings()

func _appy_settings():
	# Apply language.
	var language_name = self.get_language()
	var language = self.supported_languages[language_name]
	TranslationServer.set_locale(language)

func get_supported_languages():
	return self.supported_languages

func get_language():
	# Look for the language. Default to "English" if missing.
	var language = settings.get_value("ui", "language", "English")
	return language

func set_language(language):
	settings.set_value("ui", "language", language)
	_save_settings()

func get_settings_file_path():
	return ProjectSettings.globalize_path(self.settings_file)

func _save_settings():
	# Save the changes by overwriting the previous file.
	settings.save(settings_file)
