extends Spatial

func _ready():
	add_to_group(groups.map_editor_data)

func get_map_editor_data():
	# Return all necessery tile data.
	var map_editor_data = {
		"filename": get_filename(),
		"parent": get_parent().get_path(),
		"translation_x": translation.x,
		"translation_z": translation.z
	}
	return map_editor_data