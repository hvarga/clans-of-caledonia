FROM ubuntu:16.04

# Author and Docker Image information.
LABEL maintainer="hrvoje.varga@gmail.com"
LABEL build="docker build --network host -t hvarga/godot-docker ."
LABEL run="docker run --rm --name godot-docker -v `pwd`:/home/docker/project -u $(id -u $USER):$(id -g $USER) hvarga/godot-docker make"

# Install necessary packages.
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install --no-install-recommends -y \
    curl ca-certificates unzip make git xz-utils

# Remove cached packages.
RUN rm -rf /var/lib/apt/lists/*

# Install UPX.
RUN curl -L https://github.com/upx/upx/releases/download/v3.95/upx-3.95-amd64_linux.tar.xz -o /tmp/upx.tar.xz && \
    xz -d -c /tmp/upx.tar.xz | tar -xOf - upx-3.95-amd64_linux/upx > /bin/upx && \
    chmod a+x /bin/upx && \
    rm -rf /tmp/upx.tar.xz

# Install fixuid.
RUN curl -SsL https://github.com/boxboat/fixuid/releases/download/v0.4/fixuid-0.4-linux-amd64.tar.gz | tar -C /usr/local/bin -xzf - && \
    chown root:root /usr/local/bin/fixuid && \
    chmod 4755 /usr/local/bin/fixuid && \
    mkdir -p /etc/fixuid && \
    echo "user: docker" >> /etc/fixuid/config.yml && \
    echo "group: docker" >> /etc/fixuid/config.yml && \
    echo "paths:" >> /etc/fixuid/config.yml && \
    echo "  - /home/docker/.config" >> /etc/fixuid/config.yml && \
    echo "  - /home/docker/.cache" >> /etc/fixuid/config.yml

# Create user.
RUN addgroup --gid 1000 docker && \
    adduser --uid 1000 --ingroup docker --home /home/docker --shell /bin/zsh --disabled-password --gecos "" docker

# Install Godot Headless.
USER docker
RUN mkdir ~/.cache && \
    mkdir -p ~/.config/godot && \
    mkdir -p ~/.local/share/godot/templates/3.0.6.stable
USER root
RUN curl -L https://downloads.tuxfamily.org/godotengine/3.0.6/Godot_v3.0.6-stable_linux_headless.64.zip -o /tmp/godot.zip && \
    curl -L https://downloads.tuxfamily.org/godotengine/3.0.6/Godot_v3.0.6-stable_export_templates.tpz -o /tmp/export_templates.tpz && \
    unzip /tmp/godot.zip -d /tmp && \
    mv /tmp/Godot_v*_linux_headless.64 /bin/godot && \
    rm -f /tmp/godot.zip
USER docker
RUN unzip -d ~/.local/share/godot/templates/3.0.6.stable /tmp/export_templates.tpz && \
    mv ~/.local/share/godot/templates/3.0.6.stable/templates/* ~/.local/share/godot/templates/3.0.6.stable/ && \
    rm -rf ~/.local/share/godot/templates/3.0.6.stable/templates
USER root
RUN rm -f /tmp/export_templates.tpz

# Change user.
USER docker

# Set working directory.
WORKDIR /home/docker/project

# Fix user permissions.
ENTRYPOINT ["fixuid", "-q"]
