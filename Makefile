.PHONY: all init build_linux_x11 build_windows_desktop build_macos clean

all: clean init build_linux_x11 build_windows_desktop build_macos

init:
	mkdir builds
	touch resources/build_version.json
	echo "{" >> resources/build_version.json
	git log -1 --format='"version":"%H",' >> resources/build_version.json
	git log -1 --date=iso --format='"date":"%ad"' >> resources/build_version.json
	echo "}" >> resources/build_version.json

build_linux_x11:
	mkdir builds/linux_x11
	godot --export "linux_x11" builds/linux_x11/clans_of_caledonia

build_windows_desktop:
	mkdir builds/windows_desktop
	godot --export "windows_desktop" builds/windows_desktop/clans_of_caledonia.exe

build_macos:
	mkdir builds/macos
	godot --export "macos" builds/macos/clans_of_caledonia.zip

clean:
	rm -rf builds
	rm -rf resources/build_version.json
